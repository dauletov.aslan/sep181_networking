package kz.astana.networkingapp;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class UserEntity {
    @PrimaryKey(autoGenerate = true)
    int id;
    String name;
    String username;
    String email;
    String phone;
    String website;

    public User toUser() {
        return new User(id, name, username, email, phone, website);
    }
}
