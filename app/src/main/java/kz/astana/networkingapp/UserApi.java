package kz.astana.networkingapp;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface UserApi {
    @GET("users")
    Single<List<User>> getAllUsers();

    @POST("posts")
    Completable post(
            @Query("title") String title,
            @Query("body") String body,
            @Query("userId") String userId
    );

    @POST("posts")
    Completable post(
            @Body List<User> users
    );
}
