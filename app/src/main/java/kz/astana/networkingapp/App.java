package kz.astana.networkingapp;

import android.app.Application;

import androidx.room.Room;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {

    public static App instance;
    private AppDatabase database;
    private UserApi userApi;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;
        database = Room.databaseBuilder(this, AppDatabase.class, "database").build();

        RxJava2CallAdapterFactory adapterFactory = RxJava2CallAdapterFactory.create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(adapterFactory)
                .build();
        userApi = retrofit.create(UserApi.class);
    }

    public AppDatabase getDatabase() {
        return database;
    }

    public UserApi getUserApi() {
        return userApi;
    }
}
