package kz.astana.networkingapp;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Action;
import io.reactivex.functions.Consumer;
import io.reactivex.schedulers.Schedulers;

@SuppressLint("CheckResult")
public class MainActivity extends AppCompatActivity {

    private RecyclerViewAdapter adapter;
    private AppDatabase database;
    private UserApi userApi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(MainActivity.this));
        adapter = new RecyclerViewAdapter();
        recyclerView.setAdapter(adapter);

        database = App.instance.getDatabase();
        userApi = App.instance.getUserApi();

        userApi.post("", "", "");

        database.userDao().getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<UserEntity>>() {
                    @Override
                    public void accept(List<UserEntity> userEntities) {
                        Log.d("Hello", "onNext(Database)");
                        if (userEntities.size() > 0) {
                            List<User> users = new ArrayList<>();
                            for (UserEntity userEntity : userEntities) {
                                users.add(userEntity.toUser());
                            }
                            adapter.setItems(users);
                        } else {
                            getUsersFromInternet();
                        }
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        Log.e("Hello", "onError(Database)");
                    }
                }, new Action() {
                    @Override
                    public void run() {
                        Log.d("Hello", "onComplete(Database)");
                    }
                }, new Consumer<Subscription>() {
                    @Override
                    public void accept(Subscription subscription) {

                    }
                });
    }

    private void getUsersFromInternet() {
        userApi.getAllUsers()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Consumer<List<User>>() {
                    @Override
                    public void accept(List<User> users) {
                        Log.d("Hello", "onSuccess(Network)");
                        insertUsersToDatabase(users);
                    }
                }, new Consumer<Throwable>() {
                    @Override
                    public void accept(Throwable throwable) {
                        Log.e("Hello", "onError(Network)");
                    }
                });
    }

    private void insertUsersToDatabase(List<User> users) {
        List<UserEntity> userEntities = new ArrayList<>();
        for (User user : users) {
            UserEntity userEntity = new UserEntity();
            userEntity.id = user.getId();
            userEntity.name = user.getName();
            userEntity.username = user.getUsername();
            userEntity.email = user.getEmail();
            userEntity.phone = user.getPhone();
            userEntity.website = user.getWebsite();

            userEntities.add(userEntity);

            database.userDao().insertUsers(userEntities)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new Action() {
                        @Override
                        public void run() throws Exception {
                            Log.d("Hello", "onComplete(Insert)");
                        }
                    }, new Consumer<Throwable>() {
                        @Override
                        public void accept(Throwable throwable) throws Exception {
                            Log.e("Hello", "onError(Insert)");
                        }
                    });
        }
    }
}