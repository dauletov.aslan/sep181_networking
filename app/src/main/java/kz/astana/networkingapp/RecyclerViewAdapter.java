package kz.astana.networkingapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.RecyclerViewHolder> {

    private List<User> users;

    public RecyclerViewAdapter() {
        users = new ArrayList<>();
    }

    @NonNull
    @Override
    public RecyclerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new RecyclerViewHolder(
                LayoutInflater.from(parent.getContext()).inflate(R.layout.item_recycler_view, parent, false)
        );
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerViewHolder holder, int position) {
        User user = users.get(position);
        holder.name.setText(user.getName());
        holder.username.setText(user.getUsername());
        holder.email.setText(user.getEmail());
        holder.phone.setText(user.getPhone());
        holder.website.setText(user.getWebsite());
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void setItems(List<User> items) {
        users.clear();
        users = items;
        notifyDataSetChanged();
    }

    class RecyclerViewHolder extends RecyclerView.ViewHolder {

        public TextView name, username, email, phone, website;

        public RecyclerViewHolder(@NonNull View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.nameTextView);
            username = itemView.findViewById(R.id.usernameTextView);
            email = itemView.findViewById(R.id.emailTextView);
            phone = itemView.findViewById(R.id.phoneTextView);
            website = itemView.findViewById(R.id.websiteTextView);
        }
    }
}
